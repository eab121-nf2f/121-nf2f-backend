/**
 * @module app/index
 */

import express from 'express';

import loadRouter from './preLoader/router.loader';
import loadSecurity from './preLoader/security.loader';
import loadCors from './preLoader/cors.loader';
import loadLogger from './preLoader/logger.loader';
import loadExpressBasic from './preLoader/express.loader';
import loadDB from './preLoader/db.loader';

const app = express();

loadExpressBasic(app);
loadSecurity(app);
loadLogger(app);
loadCors(app);
loadRouter(app);
loadDB();

export default app;
