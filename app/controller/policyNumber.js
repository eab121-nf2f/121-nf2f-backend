import { insertDummyPolicyNumbers, getAvailablePolicyNumber } from '../utils/policyNumber.util';

const insertDummyPolicyNumbersToDB = async (amount) => {
  await insertDummyPolicyNumbers(amount);
  return null;
};

const getPolicyNumberFromDB = async () => {
  const result = await getAvailablePolicyNumber();
  return result;
};

export {
  insertDummyPolicyNumbersToDB,
  getPolicyNumberFromDB,
};
