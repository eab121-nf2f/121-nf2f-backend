/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @module controller/user
 */
import bcrypt from 'bcrypt';
import moment from 'moment';
import User from '../model/user';
import sendEmail from '../utils/email.util';
import getConfig from '../utils/config.util';

const saltRounds = 10;

/**
 * @function
 * @description Register user account
 * @param {string} userName - user name
 * @param {string} password - password
 * @param {string} email - email
 * @param {string} name - name
 * @returns {string} User id
 */
const register = async (userName, password, email, name) => {
  const existingUser = await User.findOne({ userName });
  if (existingUser) {
    return { error: 'User exists' };
  }
  const hashedPassword = await bcrypt.hash(password, saltRounds);
  const newUser = new User({
    userName,
    password: hashedPassword,
    email,
    name,
    role: 'agent',
  });
  const newSavedUser = await newUser.save();
  return newSavedUser[`${'_id'}`];
};

/**
 * @typedef {object} UserInfo - user information
 * @property {string} _id - user id
 * @property {string} name - name
 * @property {boolean} isFirstTimeLogin - Is this user first time login
 */

/**
 * @function
 * @description User login
 * @param {string} userName - user name
 * @param {string} password - password
 * @returns {UserInfo} User Info
 */
const login = async (userName, password) => {
  const user = await User.findOne({ userName }).lean();
  if (!user) {
    return { error: 'User name or password incorrect' };
  }
  const hashedPassword = user.password;
  const compareResult = await bcrypt.compare(password, hashedPassword);
  if (!compareResult) {
    return { error: 'User name or password incorrect' };
  }
  await User.findOneAndUpdate({ userName }, {
    lastLoginDate: moment(),
    updatedDate: moment(),
  });
  return { _id: user[`${'_id'}`], name: user.name, isFirstTimeLogin: !user.lastLoginDate };
};

const updatePassword = async (userId, newPassword) => {
  const user = await User.findById(userId).lean();
  const previousHashedPassword = user.password;
  const compareResult = await bcrypt.compare(newPassword, previousHashedPassword);
  if (compareResult) {
    return { error: 'New password should different from previous one.' };
  }
  const hashedPassword = await bcrypt.hash(newPassword, saltRounds);
  await User.findOneAndUpdate({ _id: userId }, { password: hashedPassword, updatedDate: moment() });
  return {};
};

const resetPassword = async (userName) => {
  const user = await User.findOne({ userName }).lean();
  if (user && user.email) {
    const resetPasswordContent = `Dear ${user.name},\n
Please click below link to reset your password. The link will expire after 24 hours.
[Reset password Link with image button]\n
Best Regards,
EAB Systems\n\n
THIS IS AN AUTO-GENERATED EMAIL. DO NOT REPLY `;
    sendEmail(getConfig('email.sender'), user.email, getConfig('email.resetPasswordTitle'), resetPasswordContent);
  }
  return {};
};

export {
  register,
  login,
  updatePassword,
  resetPassword,
};
