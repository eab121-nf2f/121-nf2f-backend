/**
 * @module controller/index
 */
import Product from '../model/product';

const getProductList = async () => {
  const products = await Product.find(null, 'name');
  return products;
};

export default getProductList;
