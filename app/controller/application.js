import * as _ from 'lodash';
import bcrypt from 'bcrypt';
import moment from 'moment';
import Application from '../model/application';
import Object from '../model/object';
import Cafe from '../model/cafe';
import User from '../model/user';
import getConfig from '../utils/config.util';
import sendEmail from '../utils/email.util';
import { getAvailablePolicyNumber } from '../utils/policyNumber.util';

const addPrefixZero = (number) => {
  let string = `${number}`;
  const length = 8 - string.length;
  let i = 0;
  for (;i < length; i += 1) {
    string = `0${string}`;
  }
  return `A${string}`;
};

const getNextApplicationNumber = async () => {
  const availablePolicyNumber = await Application.find();
  if (availablePolicyNumber.length > 0) {
    return `${addPrefixZero(availablePolicyNumber.length + 1)}`;
  }
  return `${addPrefixZero(1)}`;
};

const createApplication = async (body, userId) => {
  const newApp = _.cloneDeep(body);
  newApp.userId = userId;
  newApp.applicationNumber = await getNextApplicationNumber();
  const newApplication = new Application(newApp);
  const newSavedApplication = await newApplication.save();
  return newSavedApplication[`${'_id'}`];
};

const updatePdfs = async (applicationId, type, pdfs) => {
  const applicationPdfsPromises = [];
  _.forEach(pdfs, (pdf) => {
    applicationPdfsPromises.push(new Promise((resolve, reject) => {
      Object.findOneAndUpdate(
        { applicationId, type, key: pdf.key },
        { data: pdf.data, key: pdf.key },
        { upsert: true },
        (error, updateApplicationPdfResult) => {
          if (error) {
            reject(error);
          } else {
            resolve(updateApplicationPdfResult);
          }
        },
      );
    }));
  });
  await Promise.all(applicationPdfsPromises);
};

const updateApplication = async (applicationId, body) => {
  const clonedBody = _.clone(body);
  clonedBody.updatedDate = moment();
  const result = await Application.findOneAndUpdate({ _id: applicationId }, clonedBody)
    .catch(error => ({ error }));
  if (result.error) {
    return { error: result };
  }
  if (body.cafe) {
    const existingCafe = await Cafe.findOne({ applicationId });
    if (existingCafe) {
      await Cafe.findOneAndUpdate({ applicationId }, body.cafe);
    } else {
      const clonedCafe = _.cloneDeep(body.cafe);
      clonedCafe.applicationId = applicationId;
      const newCafe = new Cafe(clonedCafe);
      await newCafe.save();
    }
  }
  if (body.proposal) {
    await Object.findOneAndUpdate({ applicationId, type: 'proposal' }, { data: body.proposal });
  }
  if (body.applicationPdfs && body.applicationPdfs.length > 0) {
    await updatePdfs(applicationId, 'application', body.applicationPdfs);
  }
  if (body.supportingDocuments && body.supportingDocuments.length > 0) {
    await updatePdfs(applicationId, 'supportingDocument', body.supportingDocuments);
  }
  return result;
};

const deleteApplication = async (applicationId) => {
  const result = await Application.findOneAndUpdate({ _id: applicationId }, { status: 'D' });
  return result;
};

const getApplicationList = async (userId) => {
  const result = await Application.find({ userId }).lean();
  _.forEach(result, (item, index) => {
    result[`${index}`].id = item.applicationNumber;
    result[`${index}`] = _.omit(result[`${index}`], ['userId', 'roomSession', 'roomPassword', 'applicationNumber']);
    result[`${index}`].createdDate = moment(item.createdDate).local().format('YYYY/MM/DD HH:mm:ss');
    result[`${index}`].updatedDate = moment(item.updatedDate).local().format('YYYY/MM/DD HH:mm:ss');
  });
  return result;
};

const filterPdfByType = (objects, type, fields) => {
  const result = _.filter(objects, { type }) ? _.filter(objects, { type }) : '';
  const filteredResult = _.map(result, item => _.pick(item, fields));
  return filteredResult;
};

const getApplication = async (applicationId) => {
  const application = await Application.findById(applicationId);
  if (application) {
    const objects = await Object.find({ applicationId });
    const proposalPdf = (
      filterPdfByType(objects, 'proposal', ['data'])
      && filterPdfByType(objects, 'proposal', ['data']).length === 1
      && filterPdfByType(objects, 'proposal', ['data'])[0].data
    ) ? filterPdfByType(objects, 'proposal', ['data'])[0].data : '';
    const applicationPdfs = filterPdfByType(objects, 'application', ['key', 'data']);
    const supportingDocuments = filterPdfByType(objects, 'supportingDocument', ['key', 'data']);
    const cafe = await Cafe.findOne({ applicationId }).lean();
    const detailApplication = {
      productId: application.productId,
      clientName: application.clientName,
      cafe: _.omit(cafe, ['_id', 'createdDate', 'updatedDate', 'applicationId']),
      proposal: proposalPdf || '',
      applicationPdfs,
      supportingDocuments,
      status: application.status,
    };
    return detailApplication;
  }
  return { error: 'Application not exists' };
};

const agentInviteClient = async (applicationId, meetingName, password, emails, session) => {
  if (!applicationId || !password || !emails || !session) {
    return { error: 'request body format error' };
  }
  const existingApplication = await Application.findById(applicationId).lean();
  if (!existingApplication) {
    return { error: 'Application not exists.' };
  }
  const emailContent = `Dear customer,\n\n
Please click below link to start the online conference with your agent.
${getConfig('email.invitationFrontendUrl')}/client/login/${session}\n\nInvitation password: ${password}\n\n
Best Regards,
EAB Systems\n\n
THIS IS AN AUTO-GENERATED EMAIL. DO NOT REPLY.`;
  _.forEach(emails, (clientEmail) => {
    sendEmail(getConfig('email.sender'), clientEmail, getConfig('email.invitationTitle'), emailContent);
  });
  const saltRounds = 10;
  const hashedPassword = await bcrypt.hash(password, saltRounds);
  await Application.findOneAndUpdate(
    { _id: applicationId },
    { roomSession: session, roomPassword: hashedPassword, meetingName },
  );
  return {};
};

const joinApplicationRoom = async (session, password) => {
  if (session && password) {
    const application = await Application.findOne({ roomSession: session }).lean();
    if (!application) {
      return { error: 'Session incorrect' };
    }
    const compareResult = await bcrypt.compare(password, application.roomPassword);
    if (!compareResult) {
      return { error: 'Password incorrect' };
    }
    return { _id: application[`${'_id'}`] };
  }
  return { error: 'session and password are required' };
};

const submitApplication = async (applicationId) => {
  const existingApplication = await Application.findById(applicationId).lean();
  if (!existingApplication) {
    return { error: 'Application not exists.' };
  }
  let policyNumber;
  if (!existingApplication.policyNumber) {
    policyNumber = await getAvailablePolicyNumber();
    await Application.findOneAndUpdate({ _id: applicationId }, { status: 'S', policyNumber });
  } else {
    await Application.findOneAndUpdate({ _id: applicationId }, { status: 'S' });
  }
  const user = await User.findById(existingApplication.userId);
  const emailContent = `Dear Admin,\n
New eApplication has been submitted with below details.
Policy Number: 101-1020432
Submission Date: ${moment().format('DD MMM YYYY')}
Documents Attached: Proposal, eApp Form and Supporting Documents\n
Best Regards,
EAB Systems\n
THIS IS AN AUTO_GENERATED EMAIL. DO NOT REPLY.`;
  sendEmail(getConfig('email.sender'), user.email, getConfig('email.submitTitle'), emailContent);
  return { policyNumber: (policyNumber || existingApplication.policyNumber) };
};

const getApplicationIdBySessionId = async (sessionId) => {
  const existingApplication = await Application.findOne({ roomSession: sessionId });
  if (!existingApplication) {
    return { error: 'Application not exists' };
  }
  return {
    applicationNumber: existingApplication.applicationNumber,
    meetingName: existingApplication.meetingName,
  };
};

export {
  createApplication,
  updateApplication,
  getApplicationList,
  deleteApplication,
  getApplication,
  agentInviteClient,
  joinApplicationRoom,
  submitApplication,
  getApplicationIdBySessionId,
};
