import html2pdfLib from 'html-pdf';

const htmlToPdf = (html, pdfOptions) => {
  const options = Object.assign({
    format: 'A4',
    timeout: 60000,
  }, pdfOptions);
  console.log(`html to pdf options:${JSON.stringify(options)}`);
  console.log('Starting generate pdf from html');
  return new Promise((resolve, reject) => {
    html2pdfLib.create(html, options).toBuffer((err, buffer) => {
      if (err) {
        console.error('generate pdf from html error:', err);
        reject(err);
        return;
      }
      resolve(buffer && buffer.toString('base64'));
    });
  });
};

export default {
  htmlToPdf,
};
