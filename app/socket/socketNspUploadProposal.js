import log4jUtil from '../utils/log4j.util';

const embedSocketIoNspUploadProposal = (io) => {
  // const io = socketIo(server);
  const nsp = io.of('/eapp/upload');

  nsp.on('connection', (socket) => {
    log4jUtil.log('info', `client connected socket.io: ${socket.id}`);

    socket.on('joinRoom', (data) => {
      console.log(data);
      socket.join(data.roomId);
      console.log(socket.rooms);
      nsp.to(data.roomId).emit('newClientJoin', data);
    });

    socket.on('syncData', (data) => {
      socket.to(data.roomId).emit('receiveSyncData', data);
    });

    socket.on('scrollEvent', (data) => {
      console.log(data);
      socket.to(data.roomId).emit('scrollEventToOther', data);
    });

    socket.on('mouseClickEvent', (data) => {
      console.log(data);
      socket.to(data.roomId).emit('mouseClickEventToOther', data);
    });

    socket.on('openDropdownEvent', (data) => {
      console.log(data);
      socket.to(data.roomId).emit('openDropdownEventToOther', data);
    });

    socket.on('selectProductEvent', (data) => {
      console.log(data);
      socket.to(data.roomId).emit('selectProductEventToOther', data);
    });

    socket.on('uploadProposalPDFEvent', (data) => {
      console.log('uploadProposalPDFEvent');
      socket.to(data.roomId).emit('uploadProposalPDFEventToOther', data);
    });

    socket.on('disconnect', () => {
      log4jUtil.log('info', `client disconnected socket.io: ${socket.id}`);
    });

    socket.on('error', (error) => {
      log4jUtil.log('error', `client ${socket.id} error ${error}`);
    });
  });
};

export default embedSocketIoNspUploadProposal;
