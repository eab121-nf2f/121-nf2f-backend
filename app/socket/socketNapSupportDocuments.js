import log4jUtil from '../utils/log4j.util';

const embedSocketIoNspSupportingDocuments = (io) => {
  // const io = socketIo(server);
  const nsp = io.of('/eapp/supportdocs');

  nsp.on('connection', (socket) => {
    log4jUtil.log('info', `client connected socket.io: ${socket.id}`);

    socket.on('joinRoom', (data) => {
      console.log(data);
      socket.join(data.roomId);
      console.log(socket.rooms);
      nsp.to(data.roomId).emit('newClientJoin', data);
    });

    socket.on('scrollEvent', (data) => {
      console.log(data);
      socket.to(data.roomId).emit('scrollEventToOther', data);
    });

    socket.on('mouseClickEvent', (data) => {
      console.log(data);
      socket.to(data.roomId).emit('mouseClickEventToOther', data);
    });

    socket.on('uploadAddressFileEvent', (data) => {
      console.log('uploadAddressFileEvent');
      socket.to(data.roomId).emit('uploadAddressFileEventToOther', data);
    });

    socket.on('uploadIDCardFileEvent', (data) => {
      console.log('uploadIDCardFileEvent');
      socket.to(data.roomId).emit('uploadIDCardFileEventToOther', data);
    });

    socket.on('disconnect', () => {
      log4jUtil.log('info', `client disconnected socket.io: ${socket.id}`);
    });

    socket.on('error', (error) => {
      log4jUtil.log('error', `client ${socket.id} error ${error}`);
    });
  });
};

export default embedSocketIoNspSupportingDocuments;
