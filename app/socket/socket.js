import socketIo from 'socket.io';
import log4jUtil from '../utils/log4j.util';

const embedSocketIO = (server) => {
  const io = socketIo(server);

  io.on('connection', (socket) => {
    log4jUtil.log('info', `new client connection socket.id (${socket.id})`);

    socket.on('joinExistingRoom', (data) => {
      log4jUtil.log('info', `new client joinExistingRoom (${socket.id})`);
      const room = Object.keys(socket.rooms).find(roomId => roomId !== socket.id);
      if (room) {
        socket.leave(room);
      }
      socket.join(data.room, () => {
        socket.to(data.room).emit('newClientJoin', socket.id);
      });
    });

    socket.on('sendMessage', (data) => {
      log4jUtil.log('info', `client ${socket.id} sendMessage`);
      log4jUtil.log('info', data.room);
      log4jUtil.log('info', data.msg);
      socket.to(data.room).emit('sendMessage', data.msg);
    });

    socket.on('disconnect', () => {
      log4jUtil.log('info', `client ${socket.id} disconnection`);
    });

    socket.on('error', (error) => {
      log4jUtil.log('error', `client ${socket.id} error ${error}`);
    });
  });
};

export default embedSocketIO;
