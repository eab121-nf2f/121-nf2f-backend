import log4jUtil from '../utils/log4j.util';

const embedSocketNspChat = (io) => {
  const nsp = io.of('/eapp/chat');

  nsp.on('connection', (socket) => {
    log4jUtil.log('info', `client connected socket.io: ${socket.id}`);

    socket.on('joinChatRoomEvent', (data) => {
      console.log(data);
      socket.join(data.roomId);
      console.log(socket.rooms);
      socket.to(data.roomId).emit('newClientJoinChatRoomEvent', data);
    });

    socket.on('sendMessageEvent', (data) => {
      console.log(data);
      socket.to(data.roomId).emit('receiveMessageEvent', data);
    });

    socket.on('disconnect', () => {
      log4jUtil.log('info', `client disconnected socket.io: ${socket.id}`);
    });

    socket.on('error', (error) => {
      log4jUtil.log('error', `client ${socket.id} error ${error}`);
    });
  });
};

export default embedSocketNspChat;
