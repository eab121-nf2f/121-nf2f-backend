import log4jUtil from '../../utils/log4j.util';
import { eventListenList, eventEmitList } from './socketNspBroadTopbarEvent';

const embedSocketNspBroadTopbar = (io) => {
  const nsp = io.of('/eapp/broadcastTopBar');

  nsp.on('connection', (socket) => {
    log4jUtil.log('info', `client connected socket.io: ${socket.id}`);

    socket.on(eventListenList.JOIN_NSP, (data) => {
      console.log(data);
      socket.join(data.roomId);
      socket.to(data.roomId).emit(eventEmitList.NEW_CLIENT_JOIN, data);
    });

    socket.on(eventListenList.CLOSE_CLIENT_SHARE_MODE, (data) => {
      console.log(data);
      socket.to(data.roomId).emit(eventEmitList.CLOSE_SHARE_MODE, data);
    });

    socket.on(eventListenList.START_CLIENT_SHARE_MODE, (data) => {
      console.log(data);
      socket.to(data.roomId).emit(eventEmitList.START_SHARE_MODE, data);
    });

    socket.on(eventListenList.START_CLIENT_CONTROL_MODE, (data) => {
      console.log(data);
      socket.to(data.roomId).emit(eventEmitList.START_CONTROL_MODE, data);
    });

    socket.on(eventListenList.END_CLIENT_CONTROL_MODE, (data) => {
      console.log(data);
      socket.to(data.roomId).emit(eventEmitList.END_CONTROL_MODE, data);
    });

    socket.on('disconnect', () => {
      log4jUtil.log('info', `client disconnected socket.io: ${socket.id}`);
    });

    socket.on('error', (error) => {
      log4jUtil.log('error', `client ${socket.id} error ${error}`);
    });
  });
};

export default embedSocketNspBroadTopbar;
