import * as jwt from 'jsonwebtoken';
import getConfig from './config.util';
import log4jUtil from './log4j.util';

const verifyToken = async (req, res, next) => {
  if (req.headers && req.headers.authorization && req.headers.authorization.indexOf('Bearer ') === 0) {
    const token = req.headers.authorization.substring(7);
    if (!token) {
      log4jUtil.log('error', 'Token not exists');
      res.status(401).send('Unauthorized');
    } else {
      try {
        const result = await jwt.verify(token, getConfig('auth.secret'));
        if (!result) {
          log4jUtil.log('error', 'Token not valid');
          res.status(401).send('Unauthentized');
        } else {
          log4jUtil.log('info', 'Token valid');
          req.userId = result.id;
          next();
        }
      } catch (error) {
        res.status(401).send('Unauthentized');
      }
    }
  } else {
    log4jUtil.log('error', 'Token not exists');
    res.status(401).send('Unauthorized');
  }
};
export default {
  verifyToken,
};
