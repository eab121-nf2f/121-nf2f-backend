import PolicyNumber from '../model/policyNumber';
import log4jUtil from './log4j.util';

const getAvailablePolicyNumber = async () => {
  const availablePolicyNumber = await PolicyNumber.find({ isUsed: false });
  if (availablePolicyNumber.length > 0) {
    await PolicyNumber.findOneAndUpdate({
      number: availablePolicyNumber[0].number,
    }, { isUsed: true });
    return availablePolicyNumber[0].number;
  }
  return { error: 'Policy number exhaust.' };
};

const addPrefixZero = (number) => {
  let string = `${number}`;
  const length = 8 - string.length;
  let i = 0;
  for (;i < length; i += 1) {
    string = `0${string}`;
  }
  return `P${string}`;
};

const getNextPolicyNumber = async () => {
  const allPolicyNumber = await PolicyNumber.find();
  let nextPolicyNumber = '';
  if (allPolicyNumber.length > 0) {
    nextPolicyNumber = addPrefixZero(allPolicyNumber.length + 1);
  } else {
    nextPolicyNumber = 'P00000001';
  }
  return nextPolicyNumber;
};

const createPolicyNumber = () => new Promise((resolve) => {
  getNextPolicyNumber().then((nextPolicyNumber) => {
    log4jUtil.log('info', `${nextPolicyNumber}`);
    const newPolicyNumber = new PolicyNumber({
      number: nextPolicyNumber,
      isUsed: false,
    });
    newPolicyNumber.save(() => {
      resolve();
    });
  });
});


const insertDummyPolicyNumbers = (amount) => {
  let i = 0;
  let p = Promise.resolve();
  for (;i < Number(amount); i += 1) {
    p = p.then(() => createPolicyNumber());
  }
  return null;
};

export {
  getAvailablePolicyNumber,
  insertDummyPolicyNumbers,
};
