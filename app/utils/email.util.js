import nodemailer from 'nodemailer';
import getConfig from './config.util';

const sendEmail = async (from, to, title, content) => {
  const transporter = nodemailer.createTransport({
    host: getConfig('email.ip'),
    port: getConfig('email.port'),
    secure: false, // true for 465, false for other ports
    requireTLS: true,
    tls: {
      rejectUnauthorized: false,
    },
  });
  await transporter.sendMail({
    from,
    to,
    subject: title,
    text: content,
  });
  return {};
};

export default sendEmail;
