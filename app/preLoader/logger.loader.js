import log4js from 'log4js';
import morgan from 'morgan';

const loadLoggerMiddleware = (app) => {
  const log4j = log4js.getLogger();
  log4j.level = 'debug';

  app.use(morgan('dev'));
};

export default loadLoggerMiddleware;
