import cookieParser from 'cookie-parser';

import express from 'express';
import * as bodyParser from 'body-parser';

import getConfig from '../utils/config.util';

const loadExpressBasicMiddleware = (app) => {
  // Set maximal request body size
  // Express.js maximal request body default value is 100 kb
  // When APP request update application endpoint with pdf(base64 format), 100kb is not enough.
  // The size can be set on global variable so that we can modify it conveniently.
  // Refer to: https://www.npmjs.com/package/body-parser#limit
  app.use(bodyParser.json({ limit: getConfig('auth.requestBodyMaxSize') }));
  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));
  app.use(cookieParser());
};

export default loadExpressBasicMiddleware;
