import { generalNotFoundResponse } from '../utils/response.util';
import userRouter from '../routes/user';
import productRouter from '../routes/product';
import applicationRouter from '../routes/application';
import policyNumberRouter from '../routes/policyNumber';
import pdfRouter from '../routes/pdf';

const loadRouterMiddleware = (app) => {
  app.use('/user', userRouter);
  app.use('/product', productRouter);
  app.use('/application', applicationRouter);
  app.use('/policyNumber', policyNumberRouter);
  app.use('/pdf', pdfRouter);
  // 404 handler
  app.use((req, res) => {
    generalNotFoundResponse(res);
  });
};

export default loadRouterMiddleware;
