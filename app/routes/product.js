import express from 'express';
import { generalSuccessResponse } from '../utils/response.util';
import authUtil from '../utils/auth.util';
import getProductList from '../controller/product';

const router = express.Router();

router.get('/list', authUtil.verifyToken, async (req, res) => {
  const newUserId = await getProductList(req.body);
  generalSuccessResponse(res, newUserId);
});

export default router;
