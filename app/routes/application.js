import express from 'express';
import * as jwt from 'jsonwebtoken';
import { check, validationResult } from 'express-validator';
import getConfig from '../utils/config.util';
import {
  generalSuccessResponse,
  generalInternalServerErrorResponse,
  generalBadRequestResponse,
  customResponse,
} from '../utils/response.util';
import authUtil from '../utils/auth.util';
import {
  createApplication,
  getApplicationList,
  updateApplication,
  deleteApplication,
  getApplication,
  agentInviteClient,
  joinApplicationRoom,
  submitApplication,
  getApplicationIdBySessionId,
} from '../controller/application';

const router = express.Router();

router.post('/', authUtil.verifyToken, async (req, res) => {
  const newUserId = await createApplication(req.body, req.userId);
  generalSuccessResponse(res, newUserId);
});

router.get('/list', authUtil.verifyToken, async (req, res) => {
  const result = await getApplicationList(req.userId);
  generalSuccessResponse(res, result);
});

router.get('/session/:sessionId', async (req, res) => {
  const result = await getApplicationIdBySessionId(req.params.sessionId);
  if (result.error) {
    generalBadRequestResponse(res, result.error);
  } else {
    generalSuccessResponse(res, result);
  }
});

router.put('/:applicationId', authUtil.verifyToken, async (req, res) => {
  await updateApplication(req.params.applicationId, req.body);
  generalSuccessResponse(res, null);
});

router.get('/:applicationId', authUtil.verifyToken, async (req, res) => {
  const result = await getApplication(req.params.applicationId);
  generalSuccessResponse(res, result);
});

router.delete('/:applicationId', authUtil.verifyToken, async (req, res) => {
  await deleteApplication(req.params.applicationId);
  generalSuccessResponse(res, null);
});

router.post('/:applicationId/submit', authUtil.verifyToken, async (req, res) => {
  const policyNumber = await submitApplication(req.params.applicationId, req.body);
  generalSuccessResponse(res, policyNumber);
});

router.post('/invite', [
  check('applicationId').exists(),
  check('password').exists(),
  check('emails').exists(),
  check('session').exists(),
  check('meetingName').exists(),
], async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    customResponse(res, 422, errors);
  } else {
    const {
      applicationId, meetingName, password, emails, session,
    } = req.body;
    const result = await agentInviteClient(applicationId, meetingName, password, emails, session);
    if (result.error) {
      generalInternalServerErrorResponse(res, result);
    } else {
      generalSuccessResponse(res, null);
    }
  }
});

router.post('/join', async (req, res) => {
  const { session, password } = req.body;
  const result = await joinApplicationRoom(session, password);
  if (result.error) {
    generalBadRequestResponse(res, result);
  } else {
    const applicationId = result[`${'_id'}`].toString();
    const token = jwt.sign({ id: applicationId }, getConfig('auth.secret'), {
      expiresIn: getConfig('auth.expiryTime'),
    });
    res.cookie('t', token);
    generalSuccessResponse(res, {
      name: result.name,
      isFirstTimeLogin: result.isFirstTimeLogin,
      t: token,
    });
  }
});

export default router;
