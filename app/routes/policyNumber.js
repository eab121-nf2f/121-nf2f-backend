import express from 'express';
import { generalSuccessResponse } from '../utils/response.util';
import { insertDummyPolicyNumbersToDB, getPolicyNumberFromDB } from '../controller/policyNumber';

const router = express.Router();

router.get('/dummy/:amount', async (req, res) => {
  await insertDummyPolicyNumbersToDB(req.params.amount);
  generalSuccessResponse(res, null);
});

router.get('/', async (req, res) => {
  const number = await getPolicyNumberFromDB();
  generalSuccessResponse(res, number);
});

export default router;
