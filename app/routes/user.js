import express from 'express';
import * as jwt from 'jsonwebtoken';
import { check, validationResult } from 'express-validator';
import {
  login,
  register,
  updatePassword,
  resetPassword,
} from '../controller/user';
import { generalSuccessResponse, generalBadRequestResponse, customResponse } from '../utils/response.util';
import getConfig from '../utils/config.util';
import authUtil from '../utils/auth.util';

const router = express.Router();

router.post('/register', [
  check('userName').exists().isLength({ min: 4 }),
  check('password').exists().isLength({ min: 6 }),
  check('email').exists().isEmail(),
  check('name').exists().isLength({ min: 4 }),
], async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    customResponse(res, 422, errors);
  } else {
    const {
      userName, password, email, name,
    } = req.body;
    const response = await register(userName, password, email, name);
    if (response.error) {
      generalBadRequestResponse(res, response);
    } else {
      generalSuccessResponse(res, response);
    }
  }
});

router.post('/login', [
  check('userName').exists().isLength({ min: 4 }),
  check('password').exists(),
], async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    customResponse(res, 422, errors);
  } else {
    const { userName, password } = req.body;
    const result = await login(userName, password);
    if (result.error) {
      generalBadRequestResponse(res, result.error);
    } else {
      const userId = result[`${'_id'}`].toString();
      const token = jwt.sign({ id: userId }, getConfig('auth.secret'), {
        expiresIn: getConfig('auth.expiryTime'),
      });
      res.cookie('t', token);
      generalSuccessResponse(res, {
        name: result.name,
        isFirstTimeLogin: result.isFirstTimeLogin,
        t: token,
      });
    }
  }
});

router.put('/profile/password', authUtil.verifyToken, async (req, res) => {
  const result = await updatePassword(req.userId, req.body.password);
  generalSuccessResponse(res, result);
});

router.put('/password', async (req, res) => {
  await resetPassword(req.body.username);
  generalSuccessResponse(res, null);
});

router.post('/logout', async (req, res) => {
  res.clearCookie('t');
  generalSuccessResponse(res, null);
});

export default router;
