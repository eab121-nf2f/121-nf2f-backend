import mongoose from 'mongoose';

const cafeModel = new mongoose.Schema({
  applicationId: {
    type: String,
    required: true,
  },
  formId: {
    type: String,
    required: true,
  },
  isCompleted: {
    type: Boolean,
  },
  responseId: {
    type: String,
  },
  createdDate: {
    type: Date,
    default: Date.now,
    required: false,
  },
  updatedDate: {
    type: Date,
    default: Date.now,
    required: false,
  },
}, { collection: 'cafe', strict: false });

module.exports = mongoose.model('cafe', cafeModel);
