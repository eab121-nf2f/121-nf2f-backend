import mongoose from 'mongoose';

const policyNumberModel = new mongoose.Schema({
  number: {
    type: String,
    required: true,
  },
  isUsed: {
    type: Boolean,
    required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now,
    required: true,
  },
  updatedDate: {
    type: Date,
    default: Date.now,
    required: true,
  },
}, { collection: 'policyNumber' });

module.exports = mongoose.model('policyNumber', policyNumberModel);
