import mongoose from 'mongoose';

const applicationModel = new mongoose.Schema({
  applicationNumber: {
    type: String,
    required: true,
  },
  meetingName: {
    type: String,
  },
  userId: {
    type: String,
    required: true,
  },
  productId: {
    type: String,
  },
  clientName: {
    type: String,
  },
  roomSession: {
    type: String,
  },
  roomPassword: {
    type: String,
  },
  status: {
    type: String,
    enum: ['I', 'D', 'S'],
    default: 'I',
    required: true,
  },
  policyNumber: {
    type: String,
  },
  createdDate: {
    type: Date,
    default: Date.now,
    required: false,
  },
  updatedDate: {
    type: Date,
    default: Date.now,
    required: false,
  },
}, { collection: 'application' });

module.exports = mongoose.model('application', applicationModel);
