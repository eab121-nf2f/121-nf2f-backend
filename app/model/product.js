import mongoose from 'mongoose';

const productModel = new mongoose.Schema({
  name: {
    type: Object,
    required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now,
    required: false,
  },
  updatedDate: {
    type: Date,
    default: Date.now,
    required: false,
  },
}, { collection: 'product' });

module.exports = mongoose.model('product', productModel);
