import mongoose from 'mongoose';

const userModel = new mongoose.Schema({
  userName: {
    type: String,
  },
  password: {
    type: String,
    required: true,
  },
  email: {
    type: String,
  },
  name: {
    type: String,
  },
  role: {
    type: String,
    required: true,
  },
  lastLoginDate: {
    type: Date,
    default: null,
  },
  createdDate: {
    type: Date,
    default: Date.now,
    required: false,
  },
  updatedDate: {
    type: Date,
    default: Date.now,
    required: false,
  },
}, { collection: 'user' });

module.exports = mongoose.model('user', userModel);
