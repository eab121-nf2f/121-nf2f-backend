import mongoose from 'mongoose';

const objectModel = new mongoose.Schema({
  applicationId: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    enum: ['proposal', 'application', 'supportingDocument'],
    required: true,
  },
  key: {
    type: String,
  },
  data: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now,
    required: true,
  },
  updatedDate: {
    type: Date,
    default: Date.now,
    required: true,
  },
}, { collection: 'object' });

module.exports = mongoose.model('object', objectModel);
