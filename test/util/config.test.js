import getConfig from '../../app/utils/config.util';

describe('Config Util', () => {
  it('[function]getConfig [cause] Success', () => {
    expect(getConfig('mongodb.url')).toBe('192.168.225.180');
  });

  it('[function]getConfig [cause] key empty)', () => {
    expect(getConfig('')).toBe('');
  });

  it('[function]getConfig [cause] not exist empty)', () => {
    expect(getConfig('mongodb.abc')).toBe('');
  });
});
