import request from 'supertest';
import faker from 'faker';
import 'babel-polyfill';

import app from '../../app/index';

describe('router', () => {
  it('[router][GET] /{random path} [cause] 404 error', (done) => {
    const randomPath = faker.random.words();
    request(app)
      .get(`/${randomPath}`)
      .then((response) => {
        expect(response.status).toBe(404);
        done();
      });
  });
});
