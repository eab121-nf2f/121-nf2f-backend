import request from 'supertest';
import 'babel-polyfill';
import faker from 'faker';
import * as _ from 'lodash';
import User from '../../../app/model/user';
import app from '../../../app/index';

const testingUserName = [];

const cleanTestingData = async () => {
  console.log('Start cleaning testing data');
  const promises = [];
  _.forEach(testingUserName, (userName) => {
    promises.push(new Promise((resolve) => {
      User.findOneAndDelete({ userName }, (err, result) => {
        console.log(`cleaning user: ${userName}`);
        if (err) {
          console.error(err);
        } else {
          console.log(result);
        }
        resolve();
      });
    }));
  });
  await Promise.all(promises);
  console.log('Finish clean testing data.');
};

afterAll(async () => {
  await cleanTestingData();
});

describe('router', () => {
  it('[router][POST] /user/register [cause] success', async () => {
    const fakerUserName = faker.internet.userName();
    const fakerPassword = faker.internet.password();
    const fakerEmail = faker.internet.email();
    const fakerName = `${faker.name.firstName()} ${faker.name.lastName()}`;
    console.log(`fakerUserName:${fakerUserName}`);
    console.log(`fakerPassword:${fakerPassword}`);
    console.log(`fakerEmail:${fakerEmail}`);
    console.log(`fakerName:${fakerName}`);
    testingUserName.push(fakerUserName);
    const response = await request(app)
      .post('/user/register')
      .send({ userName: fakerUserName })
      .send({ password: fakerPassword })
      .send({ email: fakerEmail })
      .send({ name: fakerName });
    expect(response.status).toBe(200);
  });

  it('[router][POST] /user/register [cause] fail without any request body', async () => {
    const response = await request(app)
      .post('/user/register');
    expect(response.status).toBe(422);
    expect(JSON.parse(response.text)).toEqual({
      errors: [{
        msg: 'Invalid value',
        param: 'userName',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'userName',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'password',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'password',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'email',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'email',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'name',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'name',
        location: 'body',
      }],
    });
  });

  it('[router][POST] /user/register [cause] fail with only user name', async () => {
    const fakerUserName = faker.internet.userName();
    console.log(`fakerUserName:${fakerUserName}`);
    const response = await request(app)
      .post('/user/register')
      .send({ userName: fakerUserName });
    expect(response.status).toBe(422);
    expect(JSON.parse(response.text)).toEqual({
      errors: [{
        msg: 'Invalid value',
        param: 'password',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'password',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'email',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'email',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'name',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'name',
        location: 'body',
      }],
    });
  });

  it('[router][POST] /user/register [cause] fail with only password', async () => {
    const fakerPassword = faker.internet.password();
    console.log(`fakerPassword:${fakerPassword}`);
    const response = await request(app)
      .post('/user/register')
      .send({ password: fakerPassword });
    expect(response.status).toBe(422);
    expect(JSON.parse(response.text)).toEqual({
      errors: [{
        msg: 'Invalid value',
        param: 'userName',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'userName',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'email',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'email',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'name',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'name',
        location: 'body',
      }],
    });
  });

  it('[router][POST] /user/register [cause] fail with only email', async () => {
    const fakerEmail = faker.internet.email();
    console.log(`fakerEmail:${fakerEmail}`);
    const response = await request(app)
      .post('/user/register')
      .send({ email: fakerEmail });
    expect(response.status).toBe(422);
    expect(JSON.parse(response.text)).toEqual({
      errors: [{
        msg: 'Invalid value',
        param: 'userName',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'userName',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'password',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'password',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'name',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'name',
        location: 'body',
      }],
    });
  });

  it('[router][POST] /user/register [cause] fail with only name', async () => {
    const fakerName = `${faker.name.firstName()} ${faker.name.lastName()}`;
    console.log(`fakerName:${fakerName}`);
    const response = await request(app)
      .post('/user/register')
      .send({ name: fakerName });
    expect(response.status).toBe(422);
    expect(JSON.parse(response.text)).toEqual({
      errors: [{
        msg: 'Invalid value',
        param: 'userName',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'userName',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'password',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'password',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'email',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'email',
        location: 'body',
      }],
    });
  });

  it('[router][POST] /user/register [cause] fail with only user name, password', async () => {
    const fakerUserName = faker.internet.userName();
    const fakerPassword = faker.internet.password();
    console.log(`fakerUserName:${fakerUserName}`);
    console.log(`fakerPassword:${fakerPassword}`);
    const response = await request(app)
      .post('/user/register')
      .send({ userName: fakerUserName })
      .send({ password: fakerPassword });
    expect(response.status).toBe(422);
    expect(JSON.parse(response.text)).toEqual({
      errors: [{
        msg: 'Invalid value',
        param: 'email',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'email',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'name',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'name',
        location: 'body',
      }],
    });
  });

  it('[router][POST] /user/register [cause] fail with only user name, email', async () => {
    const fakerUserName = faker.internet.userName();
    const fakerEmail = faker.internet.email();
    console.log(`fakerUserName:${fakerUserName}`);
    console.log(`fakerEmail:${fakerEmail}`);
    const response = await request(app)
      .post('/user/register')
      .send({ userName: fakerUserName })
      .send({ email: fakerEmail });
    expect(response.status).toBe(422);
    expect(JSON.parse(response.text)).toEqual({
      errors: [{
        msg: 'Invalid value',
        param: 'password',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'password',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'name',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'name',
        location: 'body',
      }],
    });
  });

  it('[router][POST] /user/register [cause] fail with only user name, name', async () => {
    const fakerUserName = faker.internet.userName();
    const fakerName = `${faker.name.firstName()} ${faker.name.lastName()}`;
    console.log(`fakerUserName:${fakerUserName}`);
    console.log(`fakerName:${fakerName}`);
    const response = await request(app)
      .post('/user/register')
      .send({ userName: fakerUserName })
      .send({ name: fakerName });
    expect(response.status).toBe(422);
    expect(JSON.parse(response.text)).toEqual({
      errors: [{
        msg: 'Invalid value',
        param: 'password',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'password',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'email',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'email',
        location: 'body',
      }],
    });
  });

  it('[router][POST] /user/register [cause] fail with only password, email', async () => {
    const fakerPassword = faker.internet.password();
    const fakerEmail = faker.internet.email();
    console.log(`fakerPassword:${fakerPassword}`);
    console.log(`fakerEmail:${fakerEmail}`);
    const response = await request(app)
      .post('/user/register')
      .send({ password: fakerPassword })
      .send({ email: fakerEmail });
    expect(response.status).toBe(422);
    expect(JSON.parse(response.text)).toEqual({
      errors: [{
        msg: 'Invalid value',
        param: 'userName',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'userName',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'name',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'name',
        location: 'body',
      }],
    });
  });

  it('[router][POST] /user/register [cause] fail with only password, name', async () => {
    const fakerPassword = faker.internet.password();
    const fakerName = `${faker.name.firstName()} ${faker.name.lastName()}`;
    console.log(`fakerPassword:${fakerPassword}`);
    console.log(`fakerName:${fakerName}`);
    const response = await request(app)
      .post('/user/register')
      .send({ password: fakerPassword })
      .send({ name: fakerName });
    expect(response.status).toBe(422);
    expect(JSON.parse(response.text)).toEqual({
      errors: [{
        msg: 'Invalid value',
        param: 'userName',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'userName',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'email',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'email',
        location: 'body',
      }],
    });
  });

  it('[router][POST] /user/register [cause] fail with only email, name', async () => {
    const fakerEmail = faker.internet.email();
    const fakerName = `${faker.name.firstName()} ${faker.name.lastName()}`;
    console.log(`fakerEmail:${fakerEmail}`);
    console.log(`fakerName:${fakerName}`);
    const response = await request(app)
      .post('/user/register')
      .send({ email: fakerEmail })
      .send({ name: fakerName });
    expect(response.status).toBe(422);
    expect(JSON.parse(response.text)).toEqual({
      errors: [{
        msg: 'Invalid value',
        param: 'userName',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'userName',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'password',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'password',
        location: 'body',
      }],
    });
  });

  it('[router][POST] /user/register [cause] fail with only user name, password, email', async () => {
    const fakerUserName = faker.internet.userName();
    const fakerPassword = faker.internet.password();
    const fakerEmail = faker.internet.email();
    console.log(`fakerUserName:${fakerUserName}`);
    console.log(`fakerPassword:${fakerPassword}`);
    console.log(`fakerEmail:${fakerEmail}`);
    const response = await request(app)
      .post('/user/register')
      .send({ userName: fakerUserName })
      .send({ password: fakerPassword })
      .send({ email: fakerEmail });
    expect(response.status).toBe(422);
    expect(JSON.parse(response.text)).toEqual({
      errors: [{
        msg: 'Invalid value',
        param: 'name',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'name',
        location: 'body',
      }],
    });
  });

  it('[router][POST] /user/register [cause] fail with only user name, password, name', async () => {
    const fakerUserName = faker.internet.userName();
    const fakerPassword = faker.internet.password();
    const fakerName = `${faker.name.firstName()} ${faker.name.lastName()}`;
    console.log(`fakerUserName:${fakerUserName}`);
    console.log(`fakerPassword:${fakerPassword}`);
    console.log(`fakerName:${fakerName}`);
    const response = await request(app)
      .post('/user/register')
      .send({ password: fakerPassword })
      .send({ userName: fakerUserName })
      .send({ name: fakerName });
    expect(response.status).toBe(422);
    expect(JSON.parse(response.text)).toEqual({
      errors: [{
        msg: 'Invalid value',
        param: 'email',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'email',
        location: 'body',
      }],
    });
  });

  it('[router][POST] /user/register [cause] fail with only password, email, name', async () => {
    const fakerPassword = faker.internet.password();
    const fakerEmail = faker.internet.email();
    const fakerName = `${faker.name.firstName()} ${faker.name.lastName()}`;
    console.log(`fakerPassword:${fakerPassword}`);
    console.log(`fakerEmail:${fakerEmail}`);
    console.log(`fakerName:${fakerName}`);
    const response = await request(app)
      .post('/user/register')
      .send({ password: fakerPassword })
      .send({ email: fakerEmail })
      .send({ name: fakerName });
    expect(response.status).toBe(422);
    expect(JSON.parse(response.text)).toEqual({
      errors: [{
        msg: 'Invalid value',
        param: 'userName',
        location: 'body',
      }, {
        msg: 'Invalid value',
        param: 'userName',
        location: 'body',
      }],
    });
  });

  it('[router][POST] /user/register [cause] fail with invalid email', async () => {
    const fakerUserName = faker.internet.userName();
    const fakerPassword = faker.internet.password();
    const fakerEmail = faker.random.words();
    const fakerName = `${faker.name.firstName()} ${faker.name.lastName()}`;
    console.log(`fakerUserName:${fakerUserName}`);
    console.log(`fakerPassword:${fakerPassword}`);
    console.log(`fakerEmail:${fakerEmail}`);
    console.log(`fakerName:${fakerName}`);
    const response = await request(app)
      .post('/user/register')
      .send({ userName: fakerUserName })
      .send({ password: fakerPassword })
      .send({ email: fakerEmail })
      .send({ name: fakerName });
    expect(response.status).toBe(422);
    expect(JSON.parse(response.text)).toEqual({
      errors: [{
        msg: 'Invalid value',
        param: 'email',
        location: 'body',
        value: fakerEmail,
      }],
    });
  });

  it('[router][POST] /user/register [cause] fail with 2 valid email(separated by ;)', async () => {
    const fakerUserName = faker.internet.userName();
    const fakerPassword = faker.internet.password();
    const fakerEmail = `${faker.internet.email()};${faker.internet.email()}`;
    const fakerName = `${faker.name.firstName()} ${faker.name.lastName()}`;
    console.log(`fakerUserName:${fakerUserName}`);
    console.log(`fakerPassword:${fakerPassword}`);
    console.log(`fakerEmail:${fakerEmail}`);
    console.log(`fakerName:${fakerName}`);
    const response = await request(app)
      .post('/user/register')
      .send({ userName: fakerUserName })
      .send({ password: fakerPassword })
      .send({ email: fakerEmail })
      .send({ name: fakerName });
    expect(response.status).toBe(422);
    expect(JSON.parse(response.text)).toEqual({
      errors: [{
        msg: 'Invalid value',
        param: 'email',
        location: 'body',
        value: fakerEmail,
      }],
    });
  });

  it('[router][POST] /user/register [cause] fail with 2 valid email(separated by ,)', async () => {
    const fakerUserName = faker.internet.userName();
    const fakerPassword = faker.internet.password();
    const fakerEmail = `${faker.internet.email()},${faker.internet.email()}`;
    const fakerName = `${faker.name.firstName()} ${faker.name.lastName()}`;
    console.log(`fakerUserName:${fakerUserName}`);
    console.log(`fakerPassword:${fakerPassword}`);
    console.log(`fakerEmail:${fakerEmail}`);
    console.log(`fakerName:${fakerName}`);
    const response = await request(app)
      .post('/user/register')
      .send({ userName: fakerUserName })
      .send({ password: fakerPassword })
      .send({ email: fakerEmail })
      .send({ name: fakerName });
    expect(response.status).toBe(422);
    expect(JSON.parse(response.text)).toEqual({
      errors: [{
        msg: 'Invalid value',
        param: 'email',
        location: 'body',
        value: fakerEmail,
      }],
    });
  });

  it('[router][POST] /user/register [cause] fail with 2 valid email(separated by space)', async () => {
    const fakerUserName = faker.internet.userName();
    const fakerPassword = faker.internet.password();
    const fakerEmail = `${faker.internet.email()} ${faker.internet.email()}`;
    const fakerName = `${faker.name.firstName()} ${faker.name.lastName()}`;
    console.log(`fakerUserName:${fakerUserName}`);
    console.log(`fakerPassword:${fakerPassword}`);
    console.log(`fakerEmail:${fakerEmail}`);
    console.log(`fakerName:${fakerName}`);
    const response = await request(app)
      .post('/user/register')
      .send({ userName: fakerUserName })
      .send({ password: fakerPassword })
      .send({ email: fakerEmail })
      .send({ name: fakerName });
    expect(response.status).toBe(422);
    expect(JSON.parse(response.text)).toEqual({
      errors: [{
        msg: 'Invalid value',
        param: 'email',
        location: 'body',
        value: fakerEmail,
      }],
    });
  });

  it('[router][POST] /user/register [cause] fail with existing user', async () => {
    const fakerUserName = faker.internet.userName();
    const fakerPassword = faker.internet.password();
    const fakerEmail = faker.internet.email();
    const fakerName = `${faker.name.firstName()} ${faker.name.lastName()}`;
    console.log(`fakerUserName:${fakerUserName}`);
    console.log(`fakerPassword:${fakerPassword}`);
    console.log(`fakerEmail:${fakerEmail}`);
    console.log(`fakerName:${fakerName}`);
    const newUser = new User({
      userName: fakerUserName,
      password: fakerPassword,
      email: fakerEmail,
      name: fakerName,
      role: 'agent',
    });
    await newUser.save();
    const response = await request(app)
      .post('/user/register')
      .send({ userName: fakerUserName })
      .send({ password: fakerPassword })
      .send({ email: fakerEmail })
      .send({ name: fakerName });
    expect(response.status).toBe(400);
    expect(JSON.parse(response.text)).toEqual({ status: 'error', message: { error: 'User exists' } });
  });
});
