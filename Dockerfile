FROM node:12.2.0-alpine
EXPOSE 3000:3000
WORKDIR /app
COPY . /app
RUN npm install --loglevel error
CMD ["npm", "run", "dev"]
